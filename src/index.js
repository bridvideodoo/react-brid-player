import React from "react";
import ReactDOM from "react-dom";
import ReactBridPlayer from "./react-brid-player";

// Embed a latest video playlist
// const playlistObject = {"id":"0","mode":"latest"};

// Embed a latest video by channel playlist
// For per channel feeds - {"id":"13","mode":"channel"}

// Embed a latest video by tag playlist
// For per tag feeds - {"id":"sport","mode":"tag"}

// Embed a custom video URL
// const videoObject = {"name": "title","image": "//cdn.brid.tv/uid/partners/7612/snapshot/155268.png","source": {"streaming": "//cdn.brid.tv/live/partners/13085/streaming/889217/889217.m3u8"}};

ReactDOM.render(<ReactBridPlayer
  divId='Brid_12345'
  id='14260'
  width='640'
  height='360'
  video = '12345'
  //playlist={playlistObject}
/>, document.getElementById("root"));

