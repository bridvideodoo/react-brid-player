This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

`<ReactBridPlayer>` is a React Component for initializing client-side instances of a Brid player. Simply give `<ReactBridPlayer>` the id of your player, the ID of the HTML element in which you want the player to initialize, and a couple of other configuration parameters and you are good to go. The component comes with several event hooks that can be accessed through component props.

## Contents

- [Installation](#markdown-header-installation)
- [Usage](#markdown-header-usage)
- Props
  - [Required Props](#markdown-header-required-props)
  - Optional Props
    - [Event Hooks](#markdown-header-event-hooks)
      - [Advertising Events](#markdown-header-optional-advertising-event-hook-props)
      - [Video Events](#markdown-header-optional-video-event-hook-props)
      - [Playlist Events](#markdown-header-optional-playlist-event-hook-props)
      - [Player Events](#markdown-header-optional-player-event-hook-props)
- [Example Container Component](#markdown-header-example-container-component)
- [Contributing](#markdown-header-contributing)

## Installation

Clone this repository, navigate to the DIR where you have cloned it and type:

```shell
npm install 
```

## Usage

At the mininum, you can just use something like the below code snippet:

### Displaying a Brid Player

```javascript
import React from 'react';
import ReactDOM from 'react-dom';
import ReactBridPlayer from 'react-brid-player';

ReactDOM.render(
  <ReactBridPlayer
    divId="Brid_12345"
    id="14260"
    width="640"
    height="360"
    video='381222'
  />,
  document.getElementById('my-root-div');
);
```

For more complex interaction, check out the container component example [here](#example-container-component)

## Required Props

These are props that modify the basic behavior of the component.

- `divId`
  - The ID of the DIV in which the unit will render in. This ID is useful to have to use the player's JS API for additional manipulation.
  - Type: `string`
  - Example: `divId="brid_12345"`
- `id`
  - The ID of the unit you want to use. To get this ID for your account please see this entry [here](https://developer.brid.tv/brid-player/javascript-api-reference)
  - Type: `string`
  - Example: `id="12345"`
- `width`
  - Set the width in pixels of your ad unit. Also accepted values here are percentages like 100%.
  - Type: `string`
  - Example: `width="640"`
- `height`
  - Set the height in pixels of your ad unit. Also accepted values here are percentages like 100%.
  - Type: `string`
  - Example: `height="100%"`
  - `video`
  - This is the video ID of a video from your video library in Brid CMS.
  - Type: `string`
  - Example: `video="12345"`
  - `playlist`
  - If you want to embed a playlist inside your Brid player, pass a playlist ID as a prop or a playlist object. See examples below.
  - Type: `string`
  - Example: `playlist="12345"`
  - Example for a latest videos feed: `playlist={"id":"0","mode":"latest"}`
  - Example for a per channel video feed: `playlist={"id":"13","mode":"channel"}`
  - Example for a dynamic video feed per tag (sport): `playlist={"id":"sport","mode":"tag"}`

# Event Hooks

`react-brid-player` dynamically supports all events in Brid Player. Simply preface the event name with `on` and pass it in as a prop.

Examples:

- `requestAd` => `onRequestAd`
- `adStart` => `onAdStart`

`react-brid-player` has layered some different functionality on some of these events, so please check the docs below if you find any unexpected behavior!

## Optional Advertising Event Hook Props

- `onRequestAd(event)`
  - A function that is run when the player calls your ad tag URL.
  - Type: `function`
  - Arguments:
    - `event`
      - This is the event object passed back from Brid Player itself.
- `onAdImpression(event)`
  - A function that is run when the player fires your ads impression pixel.
  - Type: `function`
  - Arguments:
    - `event`
      - This is the event object passed back from Brid Player itself.
- `onAdFirstQuartile(event)`
  - A function that is run when the user sees 25% of your advertisement.
  - Type: `function`
  - Arguments:
    - `event`
      - This is the event object passed back from Brid Player itself.
- `onAdMidpoint(event)`
  - A function that is run when the user sees 50% of your advertisement.
  - Type: `function`
  - Arguments:
    - `event`
      - This is the event object passed back from Brid Player itself.
- `onAdThirdQuartile(event)`
  - A function that is run when the user sees 75% of your advertisement.
  - Type: `function`
  - Arguments:
    - `event`
      - This is the event object passed back from Brid Player itself.
- `onAdEnd(event)`
  - A function that is run when the user sees 100% of your advertisement.
  - Type: `function`
  - Arguments:
    - `event`
      - This is the event object passed back from Brid Player itself.
- `onAdPause(event)`
  - A function that is run when the user pauses the preroll advertisement.
  - Type: `function`
  - Arguments:
    - `event`
      - This is the event object passed back from Brid Player itself.
- `onAdStart(event)`
  - A function that is run once, when the preroll advertisement first starts to play.
  - Type: `function`
  - Arguments:
    - `event`
      - This is the event object passed back from Brid Player itself.
- `onAdResume(event)`
  - A function that is run when the user resumes playing the preroll advertisement.
  - Type: `function`
  - Arguments:
    - `event`
      - This is the event object passed back from Brid Player itself.
- `onAdError(event)`
  - A function that is run when your advertisement errors out for any reason. This includes empty VAST/VPAID respones.
  - Type: `function`
  - Arguments:
    - `event`
      - This is the event object passed back from Brid Player itself.

## Optional Video Event Hook Props

- `onStart(event)`
  - A function that is run when a video starts playback the first time only.
  - Type: `function`
  - Arguments:
    - `event`
      - This is the event object passed back from Brid Player itself.

- `onBeforeStart(event)`
  - A function that is run right before an ad starts playback or before a video starts playback if no ad exists.
  - Type: `function`
  - Arguments:
    - `event`
      - This is the event object passed back from Brid Player itself.

- `onPlay(event)`
  - A function that is run whenever a video begins playback.
  - Type: `function`
  - Arguments:
    - `event`
      - This is the event object passed back from Brid Player itself.

- `onPause(event)`
  - A function that is run whenever a video is paused.
  - Type: `function`
  - Arguments:
    - `event`
      - This is the event object passed back from Brid Player itself.

- `onEnded(event)`
  - A function that is run when a video finishes playback.
  - Type: `function`
  - Arguments:
    - `event`
      - This is the event object passed back from Brid Player itself.

- `onLastSecond(event)`
  - A function that is run on the last second of a video.
  - Type: `function`
  - Arguments:
    - `event`
      - This is the event object passed back from Brid Player itself.

## Optional Playlist Event Hook Props

- `onPlaylistStart(event)`
  - A function that is run whenever a playlist starts playback.
  - Type: `function`
  - Arguments:
    - `event`
      - This is the event object passed back from Brid Player itself.

- `onSkipped(event)`
  - A function that is run whenever a user clicks on the previous or next buttons on the player to go to a different video in a playlist.
  - Type: `function`
  - Arguments:
    - `event`
      - This is the event object passed back from Brid Player itself.

- `onPlaylistComplete(event)`
  - A function that is run when an entire playlist of videos has finished playback.
  - Type: `function`
  - Arguments:
    - `event`
      - This is the event object passed back from Brid Player itself.

## Optional Player Event Hook Props

- `onFullScreen(event)`
  - A function that is run when a user clicks on the player's fullscreen button.
  - Type: `function`
  - Arguments:
    - `event`
      - This is the event object passed back from Brid Player itself.
- `onMuteChange(event)`
  - A function that is run when a user changes the sound state of the player.
  - Type: `function`
  - Arguments:
    - `event`
      - This is the event object passed back from Brid Player itself.

## Example Container Component

```javascript
import React from "react";
import ReactBridOutstreamPlayer from "react-brid-player";

const displayName = "ReactBridContainer";

class ReactBridContainer extends React.Component {
  constructor(props) {
    super(props);

    this.onAdStart = this.onAdStart.bind(this);
    this.onAdImpression = this.onAdImpression.bind(this);

    this.divId = someFunctionToRandomlyGenerateId();
  }
  onAdStart(event) {
    // Do some custom code here once an ad starts playback
  }
  onAdImpression(event) {
    // Track ad impressions here
  }

  render() {
    return (
      <div className="react-brid-container">
        <ReactBridPlayer
          divId={this.divId}
          id="5817"
          width="640"
          height="360"
          onAdStart={this.onAdStart}
          onAdImpression={this.onAdImpression}
          video="12345"
        />
      </div>
    );
  }
}

ReactBridContainer.displayName = displayName;
export default ReactBridContainer;
```

## Contributing

First, thank you for taking the time to contribute something to this Brid react component!
Your help is always welcome! Feel free to open issues, ask questions, talk about it and discuss.

### Pull Requests

When contributing to this repo, please first discuss the change you wish to make via issue, email, or any other method.

### Feature Requests

I am always interested in expanding the feature-set of this component so if there is something you need, just open a ticket [here](https://brid.zendesk.com/hc/en-us/requests/new) or [drop me a line](mailto:support@brid.tv).
